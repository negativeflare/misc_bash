#!/bin/bash
# Joey Docker Stop Script
# Written by Jesse N. Richardson [negativeflare] (jr.fire.flare@gmail.com)
# Script Purpose:
# Stops the Seafile Docker Instances

# Root Check
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   echo "Run this with sudo bash <filename>"
   exit 1
fi

echo "Stopping Seafile Docker Containers"
docker stop 6587e5aea4f6 49ca92a18d41 f2d9b3d6d4fd
echo "Seafile Stopped"
