#!/bin/bash
# Tmux Monitoring Startup Script
# Written by Jesse N. Richardson (jr.fire.flare@gmail.com) [negativeflare]
# Tmux script that fires up a tmux session, ssh's into each machine and fires up bpytop on each machine.
# Pane Meanings:
     # Token                  Meaning
     # {last}            !    The last (previously active) pane
     # {next}            +    The next pane by number
     # {previous}        -    The previous pane by number
     # {top}                  The top pane
     # {bottom}               The bottom pane
     # {left}                 The leftmost pane
     # {right}                The rightmost pane
     # {top-left}             The top-left pane
     # {top-right}            The top-right pane
     # {bottom-left}          The bottom-left pane
     # {bottom-right}         The bottom-right pane
     # {up-of}                The pane above the active pane
     # {down-of}              The pane below the active pane
     # {left-of}              The pane to the left of the active pane
     # {right-of}             The pane to the right of the active pane

# Primary Window
# Servers: Onyx, Jesse-Desktop, iolite, typhlosion
tmux new -d -s monitoring
tmux rename-window -t 1 Main
sleep 1
tmux send-keys -t monitoring ssh\ onyx C-m
sleep 5
tmux send-keys -t monitoring bpytop C-m
sleep 5
tmux split-window -t monitoring
tmux split-window -h -t {last}
tmux send-keys -t monitoring ssh\ jesse-desktop C-m
sleep 5
tmux send-keys -t monitoring bpytop C-m
sleep 5
tmux select-pane -t {bottom-left}
tmux send-keys ssh\ iolite C-m
sleep 5
tmux send bpytop C-m
sleep 10
tmux split-window -h
tmux send ssh\ typhlosion C-m
sleep 5
tmux send bpytop C-m
sleep 5

# Secondary Window
# Servers: kvm (localhost)
tmux new-window
tmux rename-window -t 2 Secondary
sleep 5
tmux send bpytop C-m
sleep 5

# KVM Window
tmux new-window
tmux rename-window -t 3 KVM
tmux send ssh\ kyanite C-m
sleep 5
tmux send bpytop C-m
sleep 5
tmux split-window -h
tmux send ssh\ laserblue C-m
sleep 5
tmux send bpytop C-m
sleep 5
tmux split-window -v
tmux send ssh\ mobile-virt C-m
sleep 5
tmux send bpytop C-m
